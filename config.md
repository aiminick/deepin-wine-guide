##	15.4.1 容器介绍

    Wine容器是模拟了一个Windows系统的运行环境，对于Windows程序它就是一个Windows系统。容器基本的目录结构如下图：

![bottle_tree](./source/bottle_tree.png)

    *   dosdevices: 一些软链接文件，主要左右指定容器C盘的位置。图中还指定了Z盘是系统的根目录，这样容器中运行的Windows程序就能通过Z盘访问到系统中所有的文件。

    *   drive_c: 容器的C盘目录，在wine运行的时候自动创建的目录，可以看出和真正的Windows系统基本一样。实际目录结构比上图的要多一些。

    *   Windows目录存放系统文件，最重要的就是system32目录，存放系统最基本的动态库和一些执行文件。

    *   Program Files目录存放安装的软件，一般情况下软件都是安装在这个位置。

    *   users目录存放用户的数据。users目录还有一些软链接指定到Linux系统的用户目录，可以将Windows程序用户数据存放到Linux的系统目录，这样保证容器目录被删除之后用户数据还在。

    *   *.reg 注册表文件，存放系统注册表信息的文件。

##	15.4.2 wine的配置与使用

### 15.4.2.1 winecfg配置

    运行WINEPREFIX=~/.bottle deepin-wine winecfg可以打开配置容器的界面，WINEPREFIX环境变量指定容器路径，WINEPREFIX=~/.bottle指定的就是HOME目录的.bottle目录。

#### 15.4.2.1.1 应用程序设置

![winecfg-app](./source/winecfg-app.png)

    如上图，winecfg的第一个标签就是应用程序设置。这个设置的作用是配置应用程序运行在什么版本的系统上面，如图中下拉列表可以选择Win7、Win8、Win10等所有Windows的版本。默认设置对容器中所有的应用有效，还可以对指定应用程序设置，如图中可以设置test.exe程序运行在Win10的系统上面。

#### 15.4.2.1.2 函数库设置

![winecfg-dll](./source/winecfg-dll.png)

    如上图，winecfg第二个标签就是函数库设置。这个设置的作用是配置函数库或者执行文件加载的逻辑，如图可以配置五种情况：

    *   内建：内建的意思是wine编译提供的函数库。如果usp10配置内建，就只会加载wine编译的usp10函数库。

    *   原生：原生是相对于内建的，意思是Windows版本的函数库。如果usp10配置为原生，就只会加载Windows版本的usp10函数库，如果不拷贝Windows版本usp10.dll到容器的Windows/system32目录，加载usp10的时候就会报错。

    *   内建先于原生：意思是先尝试加载wine编译的函数库，如果失败再加载Windows版本的函数库。

    *   原生先于内建：意思是先尝试加载Windows版本函数库，如果失败再加载wine编译的函数库。这种情况下没有拷贝Windows版本的函数库，加载的过程也不会失败。

    *   停用：意思是不加载指定的函数库。

    实际应用中主要有两个场景：

    *   wine实现的函数库有问题需要用Windows版本的函数库替换。比较常用的就是riched20.dll，配置riched20为原生并且拷贝Windows版本riched20.dll到容器的Windows/system32目录可以解决输入框不能输入的问题。

    *   禁用可执行文件。在适配Windows程序的过程中会有一些不影响程序使用但是运行会报错的程序，可以通过设置停用对应的执行文件。如可以停用某些程序的更新程序(update.exe)可以起到禁用自动升级的功能。

#### 15.4.2.1.3 显示设置

![winecfg-display](./source/winecfg-display.png)

    如上图，winecfg第三个标签是显示设置。这个主要是对窗口的显示效果做一些设置，一般用户不需要去配置。可能有如下几种应用场景：

    *   窗口标题栏关闭、最大化、最小化按钮显示不对的情况，可以尝试去掉 允许窗口管理器修饰窗口 的勾选状态

    *   窗口显示不正常，如有窗口不显示、异常关闭、托盘点击无效等问题可以尝试打开虚拟桌面的选项，看运行是否正常。但是这个一般只作为分析问题对比的方法，一般不作为最终运行的方案，因为显示效果不好。

    *   设置窗口分辨率，在高分屏上面需要将dpi的值设置大一些。deepin-wine不需要用户手动设置，默认情况下deepin-wine会根据系统设置的缩放比设置分辨率的大小，达到自适应的效果。

#### 15.4.2.1.4 桌面整合设置

![winecfg-style](./source/winecfg-style.png)

    如上图，winecfg第四个标签是桌面整合设置。一般用户也不需要去配置。目前deepin-wine维护应用也只用到一个主题设置的配置，感兴趣的可以参考下：

    安装商店的wine版本QQ，运行之后拷贝~/.deepinwine/Deepin-QQ/drive_c/windows/Resources/Themes/deepin/deepin.msstyles文件到桌面，如上图点击 安装主题 按钮选择deepin.msstyles文件。然后在主题的下拉列表中选择deepin，应用设置。应用主题之后创建系统默认的窗口就会显示对应主题的样式，如下图：

![winecfg-style-2](./source/winecfg-style-2.png)

#### 15.4.2.1.5 驱动器设置

![winecfg-drive](./source/winecfg-drive.png)

    如上图，winecfg第五个标签是驱动器设置。作用是设置容器的盘符，可以添加D、E、F等盘符。在有些应用程序可能会直接将文件创建到D盘，这个情况就需要手动创建一个D盘。
    如上图选择添加然后选择添加的盘符名字，之后修改对应的路径到当前用户目录下面，保证能够读写。

### 15.4.2.2 其他配置

#### 15.4.2.2.1 字体配置

    迁移Windows应用最常见的问题之一就是界面文字显示不正常，一般都是缺少Windows对应的字体导致的。缺少字体的情况可以通过配置容器的方式解决，有如下两种解决方案：

    1.  将Windows下的字体文件拷贝到容器的windows/Fonts目录，一般将windows的宋体字体文件拷贝到这个目录就能解决大多数问题。

    2.  修改注册表：执行WINEPREFIX=~/.bottle deepin-wine regedit 打开注册表编辑器，如下图在HKEY_CURRENT_USER\Software\Wine\Fonts\Replacements键值中添加缺失字体的替换规则,下图中是将宋体替换成系统中存在的字体：

![font-reg](./source/font-reg.png)

